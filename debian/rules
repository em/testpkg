#!/usr/bin/make -f
package := $(shell dpkg-parsechangelog -S Source)
docdir = debian/tmp/usr/share/doc/$(package)
pause = 20m

print-buildenv:
	@echo 📝 Environment 📝
	env --null | sort -z | tr '\0' '\n'
	@echo 📝 Current directory 📝
	pwd

clean:
	rm -rf *~ debian/tmp debian/*~ debian/files* debian/substvars

binary-indep: print-buildenv
	rm -rf debian/tmp
	install -d debian/tmp/DEBIAN $(docdir) debian/tmp/usr/share/$(package)
	@echo "💤 Sleep for $(pause) (current time $$(date -Im)) without any output to check that the builder does not assume the build is stuck 💤"
	sleep "$(pause)"
	@echo "⏰ Done sleeping ⏰"
	for SIZE in 2G; do \
	    echo "📦 preparing $$SIZE of random data to fill the binary packages 📦"; \
	    head -c "$$SIZE" /dev/urandom > "debian/tmp/usr/share/$(package)/$$SIZE"; \
	done
	cp -a debian/copyright $(docdir)
	cp -a debian/changelog $(docdir)/changelog.Debian
	cd $(docdir) && gzip -9n changelog.Debian
	dpkg-gencontrol
	dpkg-deb --root-owner-group --build debian/tmp ..

binary-arch: print-buildenv

binary: binary-indep binary-arch

.PHONY: binary binary-arch binary-indep build-arch build-indep clean
